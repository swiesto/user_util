<?php
	error_reporting(E_ERROR);
	
	echo "\nSTART USER_TEXT_UTIL:\n\n";
	$params = array(
		''      => 'help',
		'd:'    => 'delimiter',
		'a:'    => 'action',
	);
	
	$delimiter = null;
	$action = null;
	$errors = array();
	
	try {	
		$options = getopt(implode('', array_keys($params)), $params);
		
		if(isset($options['delimiter']) || isset($options['d'])) {
			$delimiter = isset($options['delimiter']) ? $options['delimiter'] : $options['d'];
		} else {
			$errors[] = "\t delimiter is required";
		}	
		
		if(isset($options['action']) || isset($options['a'])) {
			$action = isset($options['action']) ? $options['action'] : $options['a'];
		} else {
			$errors[] = "\t action is required";
		}
		
		if(isset($options['help']) || count($errors)) {
			$help = "Example: php user_text_util.php -d=semicolon -a=countAverageLineCount \n";
			$help .= "Params:\n\t d: comma | semicolon \n\t a: countAverageLineCount | replaceDates\n";
			if(count($errors)) {
				$help .= 'Errors:' . PHP_EOL . implode("\n", $errors) . PHP_EOL;
			}
			die($help);
		}

		if(textAction($delimiter, $action) == false){
			throw new Exception("ERR:004. Invalid params, check help:'php user_text_util.php -h' \n ");
		}
 	} catch (Exception $e) {
		echo $e->getMessage();
	} 			
	
	// determinate delimiter and action type
	function textAction($delimiter, $action) {
		switch($delimiter)
		{
			case 'comma' : $delimiter = ",";
				break;
			case 'semicolon' : $delimiter = ";";
				break;
			default : $delimiter = '';
		}
		if($delimiter) {
			$result = false;
			$users = [];
 			switch($action)
			{
				case 'countAverageLineCount' : $users = getUsers($delimiter);
					$result = countAverageLineCount($users);
					break;
				case 'replaceDates' : $users = getUsers($delimiter);
					$result = replaceDates($users);
					break;
				default : $result = false;
			}
			return $result;				
		}
	}
	
	// return all users from csv	
	function getUsers($delimiter) {
		try{
			if(($handle = fopen("people.csv", "r")) !== false) {
				$i=0;
				$users = [];
				while(($data = fgetcsv($handle, 1000, $delimiter)) !== false) {
					$users[$i] = $data;
					$i++;
				}
				fclose($handle);
				if($users) {
					return $users;
				} else {
					return false;
				}
			}
			if($handle === false){
				throw new Exception("ERR:Cannot open CSV file\n");
			}
		} 
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function countAverageLineCount($users){
		$files = scandir("texts");	
		for($i = 0; $i != count($users); $i++){
			$num_files = 0;
			$num_string = 0;
			for($j = 0; $j != count($files); $j++){
				if(strstr($files[$j], $users[$i][0]."-")) {
					try{
						$handle = file("texts/".$files[$j]);
						if($handle === false){
							throw new Exception("ERR:001. Cannot read user file\n");
						}
					} 
					catch (Exception $e) {
						echo $e->getMessage();
					}
					$num_string += count($handle);
					$num_files++;
				}
			}
			if($num_files) {
				echo "For ".$users[$i][1]." middle string numbes is {".$num_string/$num_files."}\n";
			}
		}
		return true;
	}

	function replaceDates($users) {
		$files = scandir("texts");	
		for($i = 0; $i != count($users); $i++){
			$num_replace = 0;
			for($j = 0; $j != count($files); $j++){
				if(strstr($files[$j], $users[$i][0]."-")) {
					try {
						$string = file_get_contents("texts/".$files[$j], true);
						if($string === false){
							throw new Exception("ERR:002.Cannot read user file\n");
						}
					} 
					catch (Exception $e) {
						echo $e->getMessage();
					}
					$patterns = array ("/(\d{2})\/(\d{2})\/(\d{2})/");
					$replace = array ("\\1-\\2-\\3");
					$temp_string = preg_replace($patterns, $replace, $string, -1, $count);
					if($count){
						$num_replace += $count;
					}
					try {
						$handle = file_put_contents("output_texts/".$files[$j], $temp_string);
						if($handle === false){
							throw new Exception("ERR:002.Cannot create user file\n");
						}
					} 
					catch (Exception $e) {
						echo $e->getMessage();
					}
				}
			}
			if($num_replace) {
				echo "For ".$users[$i][1]." replace is {".$num_replace."}\n";
			}
		}
		echo "Output users files was update\n";
		return true;
	}